import { GraphQLObjectType } from 'graphql';
import CategoriesQueries from './categories/CategoriesQueries';
import ContractorsQueries from './contractors/ContractorsQueries';
import CustomersQueries from './customers/CustomersQueries';
import DepartmentsQueries from './departments/DepartmentsQueries';
import LogsQueries from './logs/LogsQueries';
import OrdersQueries from './orders/OrdersQueries';
import RolesQueries from './roles/RolesQueries';
import UsersQueries from './users/UsersQueries';

const Query = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    categories: CategoriesQueries,
    contractors: ContractorsQueries,
    customers: CustomersQueries,
    departments: DepartmentsQueries,
    logs: LogsQueries,
    orders: OrdersQueries,
    roles: RolesQueries,
    users: UsersQueries,
  }),
});

export default Query;
