import {
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { RoleType } from "../roles/RolesTypes";
import { RolesModel } from "../roles/RolesModel";
import { success as signaleSuccess } from "signale";

export const DepartmentType = new GraphQLObjectType({
  name: "DepartmentType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    defaultRules: {
      type: new GraphQLList(GraphQLID)
    },
    roles: {
      type: new GraphQLList(RoleType),
      async resolve(root) {
        signaleSuccess(root.roles);
        return await RolesModel.find({ _id: { $in: root.roles } });
      }
    }
  }
});

export const DepartmentCreateType = new GraphQLInputObjectType({
  name: "DepartmentCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    defaultRules: {
      type: new GraphQLList(GraphQLID)
    },
    roles: {
      type: new GraphQLList(GraphQLID)
    }
  }
});

export const DepartmentUpdateType = new GraphQLInputObjectType({
  name: "DepartmentUpdateType",
  fields: {
    name: {
      type: GraphQLString
    },
    defaultRules: {
      type: new GraphQLList(GraphQLID)
    },
    roles: {
      type: new GraphQLList(GraphQLID)
    }
  }
});
