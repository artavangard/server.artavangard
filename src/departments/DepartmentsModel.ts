import * as mongoose from 'mongoose';

export interface IDepartments extends mongoose.Document {
  name: string;
  defaultRules: string;
  roles: string[];
}
const Schema = mongoose.Schema;

const DepartmentsSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  defaultRules: {
    type: [Schema.Types.ObjectId],
    default: [],
  },
  roles: {
    type: [Schema.Types.ObjectId],
  },
});

export const DepartmentsModel = mongoose.model<IDepartments>('Departments', DepartmentsSchema);
