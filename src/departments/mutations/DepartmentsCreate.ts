import { GraphQLID, GraphQLNonNull } from "graphql";

import { DepartmentCreateType } from "../DepartmentsTypes";
import { DepartmentsModel } from "../DepartmentsModel";
import { FieldConfigWithArgs } from "../../types";

const DepartmentsCreate: FieldConfigWithArgs = {
  description: "create new department",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(DepartmentCreateType)
    }
  },
  async resolve(_, { data }) {
    const newDepartment = await new DepartmentsModel(data).save();
    if (!newDepartment) {
      return new Error("Error adding new depart");
    }

    return newDepartment._id;
  }
};

export default DepartmentsCreate;
