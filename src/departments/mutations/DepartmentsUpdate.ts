import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { DepartmentUpdateType } from "../DepartmentsTypes";
import { DepartmentsModel } from "../DepartmentsModel";
import { FieldConfigWithArgs } from "../../types";

const DepartmentsUpdate: FieldConfigWithArgs = {
  description: "update department data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(DepartmentUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await DepartmentsModel.update({ _id }, data);
    return true;
  }
};

export default DepartmentsUpdate;
