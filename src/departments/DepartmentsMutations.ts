import DepartmentsCreate from "./mutations/DepartmentsCreate";
import DepartmentsUpdate from "./mutations/DepartmentsUpdate";
import { GraphQLObjectType } from "graphql";

const DepartmentsMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "DepartmentsMutations",
    fields: {
      create: DepartmentsCreate,
      department: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "DepartmentMutations",
          fields: {
            update: DepartmentsUpdate
          }
        })
      }
    }
  })
};

export default DepartmentsMutations;
