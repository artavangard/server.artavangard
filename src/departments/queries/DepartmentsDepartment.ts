import { GraphQLID, GraphQLNonNull } from "graphql";

import { DepartmentType } from "../DepartmentsTypes";
import { DepartmentsModel } from "../DepartmentsModel";
import { FieldConfigWithArgs } from "../../types";

const DepartmentsDepartment: FieldConfigWithArgs = {
  description: "find department by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  type: new GraphQLNonNull(DepartmentType),
  async resolve(_, { _id }) {
    return await DepartmentsModel.findOne({ _id });
  }
};

export default DepartmentsDepartment;
