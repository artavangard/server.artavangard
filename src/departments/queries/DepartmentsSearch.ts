import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { DepartmentType } from "../DepartmentsTypes";
import { DepartmentsModel } from "../DepartmentsModel";
import { FieldConfigWithArgs } from "../../types";

const DepartmentsSearch: FieldConfigWithArgs = {
  description: "find departments",
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "DepartmentsSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(new GraphQLList(DepartmentType))
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "DepartmentsSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve(_, { limit, after, sort, order }) {
    const find: any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await DepartmentsModel.find().count();

    const result = await await DepartmentsModel.find(find)
      .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
      .limit(limit as any)
      .exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default DepartmentsSearch;
