import DepartmentsDepartment from "./queries/DepartmentsDepartment";
import DepartmentsSearch from "./queries/DepartmentsSearch";
import { GraphQLObjectType } from "graphql";

const DepartmentsQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "DepartmentsQueries",
    fields: {
      department: DepartmentsDepartment,
      search: DepartmentsSearch
    }
  })
};

export default DepartmentsQueries;
