import { GraphQLObjectType } from "graphql";
import RolesRole from "./queries/RolesRole";
import RolesSearch from "./queries/RolesSearch";

const RolesQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "RolesQueries",
    fields: {
      role: RolesRole,
      search: RolesSearch
    }
  })
};

export default RolesQueries;
