import { GraphQLObjectType } from "graphql";
import RolesCreate from "./mutations/RolesCreate";
import RolesUpdate from "./mutations/RolesUpdate";

const RolesMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "RolesMutations",
    fields: {
      create: RolesCreate,
      role: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "RoleMutations",
          fields: {
            update: RolesUpdate
          }
        })
      }
    }
  })
};

export default RolesMutations;
