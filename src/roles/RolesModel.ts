import * as mongoose from "mongoose";

export interface IRoles extends mongoose.Document {
  name: string;
  archive: boolean;
}

const RolesSchema = new mongoose.Schema({
  name: {
    type: String
  },
  archive: {
    type: Boolean
  }
});

export const RolesModel = mongoose.model<IRoles>("Roles", RolesSchema);
