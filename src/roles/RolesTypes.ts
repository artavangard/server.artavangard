import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

export const RoleType = new GraphQLObjectType({
  name: "RoleType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    archive: {
      type: GraphQLBoolean
    }
  }
});

export const RoleCreateType = new GraphQLInputObjectType({
  name: "RoleCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    }
  }
});

export const RoleUpdateType = new GraphQLInputObjectType({
  name: "RoleUpdateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    archive: {
      type: GraphQLBoolean
    }
  }
});
