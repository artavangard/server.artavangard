import { GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { RoleCreateType } from "../RolesTypes";
import { RolesModel } from "../RolesModel";

const RolesCreate: FieldConfigWithArgs = {
  description: "create new role",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(RoleCreateType)
    }
  },
  async resolve(_, { data }) {
    const newRole = await new RolesModel(data).save();
    if (!newRole) {
      return new Error("Error adding new role");
    }

    return newRole._id;
  }
};

export default RolesCreate;
