import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { RoleUpdateType } from "../RolesTypes";
import { RolesModel } from "../RolesModel";

const RolesUpdate: FieldConfigWithArgs = {
  description: "update role data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(RoleUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await RolesModel.update({ _id }, data);
    return true;
  }
};

export default RolesUpdate;
