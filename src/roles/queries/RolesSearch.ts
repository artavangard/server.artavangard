import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { RoleType } from "../RolesTypes";
import { RolesModel } from "../RolesModel";

const RolesSearch: FieldConfigWithArgs = {
  description: "find role",
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "RolesSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(new GraphQLList(RoleType))
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "RolesSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve(_, { limit, after, sort, order }) {
    const find: any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await RolesModel.find().count();

    const result = await await RolesModel.find(find)
      .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
      .limit(limit as any)
      .exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default RolesSearch;
