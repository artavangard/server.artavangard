import { GraphQLNonNull, GraphQLString } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { RoleType } from "../RolesTypes";
import { RolesModel } from "../RolesModel";

const RolesRole: FieldConfigWithArgs = {
  description: "find role by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  type: new GraphQLNonNull(RoleType),
  async resolve(_, { _id }) {
    return await RolesModel.findOne(_id);
  }
};

export default RolesRole;
