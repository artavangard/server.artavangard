import * as bcrypt from "bcrypt";

import { Context } from "koa";
import { RolesModel } from "../roles/RolesModel";
import { UsersModel } from "../users/UsersModel";

const createUid = async (): Promise<string> => {
  return await bcrypt.hash(
    (Math.random() * 10000).toString(32) + Date.now(),
    10
  );
};

export const SignAuthAction = async (ctx: Context) => {
  if (!ctx.cookies.get("uid")) {
    ctx.cookies.set("uid", await createUid(), {
      expires: new Date(Date.now() + 24 * 60 * 60 * 1000)
    });
    return (ctx.body = { code: 403, message: "not authorized" });
  }

  const user = await UsersModel.findOne({
    "secret.uid": ctx.cookies.get("uid")
  });

  if (!user) {
    return (ctx.body = { code: 403, message: "user not found" });
  }

  const role = await RolesModel.findOne({
    _id: user.role
  });

  return (ctx.body = {
    uid: ctx.cookies.get("uid"),
    user: {
      role: (role && role.name) || "",
      id: user._id,
      name: user.lastName + " " + user.firstName,
      rules: null
    },
    code: 200,
    message: "success"
  });
};

export const SignInAction = async (ctx: Context) => {
  const { fields } = ctx.request.body as {
    fields: { login?: string; password?: string };
  };
  const { login } = fields;
  if (!login) {
    return (ctx.body = {
      code: 403,
      error: { message: "Укажите логин для входа", field: "login" }
    });
  }

  if (!fields.password) {
    return (ctx.body = {
      code: 403,
      error: { message: "Укажите пароль для входа", field: "password" }
    });
  }

  const user = await UsersModel.findOne({ login: fields.login });
  if (!user) {
    return (ctx.body = {
      code: 403,
      error: { message: "Пользователь не найден", field: "login" }
    });
  }

  if (!(await bcrypt.compare(fields.password, user.secret.password))) {
    return (ctx.body = {
      code: 403,
      error: { message: "Неправильный пароль", field: "password" }
    });
  }

  await UsersModel.update(
    { login: fields.login },
    { $set: { "secret.uid": ctx.cookies.get("uid") } }
  );

  return (ctx.body = {
    uid: ctx.cookies.get("uid"),
    code: 200,
    user: {
      id: user._id,
      name: user.lastName + " " + user.firstName,
      rules: null
    }
  });
};

export const SignOutAction = async (ctx: Context) => {
  await UsersModel.update(
    { hash: ctx.cookies.get("uid") },
    { $set: { "secret.uid": "" } }
  );
  ctx.body = {};
};
