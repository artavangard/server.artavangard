import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { CategoriesModel } from "../CategoriesModel";
import { CategoryType } from "../CategoriesTypes";
import { FieldConfigWithArgs } from "../../types";

const CategoriesSearch: FieldConfigWithArgs = {
  description: "find categories",
  args: {
    limit: {
      type: GraphQLInt
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "CategoriesSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(new GraphQLList(CategoryType))
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "CategoriesSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve() {
    const count = await CategoriesModel.find().count();

    const result = await await CategoriesModel.find({ parent_id: null }).exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default CategoriesSearch;
