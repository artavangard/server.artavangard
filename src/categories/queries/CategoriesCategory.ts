import { GraphQLNonNull, GraphQLString } from "graphql";

import { CategoriesModel } from "../CategoriesModel";
import { CategoryType } from "../CategoriesTypes";
import { FieldConfigWithArgs } from "../../types";

const CategoriesCategory: FieldConfigWithArgs = {
  description: "find category by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  type: new GraphQLNonNull(CategoryType),
  async resolve(_, { _id }) {
    return await CategoriesModel.findById(_id);
  }
};

export default CategoriesCategory;
