import CategoriesCreate from "./mutations/CategoriesCreate";
import CategoriesUpdate from "./mutations/CategoriesUpdate";
import { GraphQLObjectType } from "graphql";

const CategoriesMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "CategoriesMutations",
    fields: {
      create: CategoriesCreate,
      category: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "CategoryMutations",
          fields: {
            update: CategoriesUpdate
          }
        })
      }
    }
  })
};

export default CategoriesMutations;
