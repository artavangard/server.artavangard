import CategoriesCategory from "./queries/CategoriesCategory";
import CategoriesSearch from "./queries/CategoriesSearch";
import { GraphQLObjectType } from "graphql";

const CategoriesQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "CategoriesQueries",
    fields: {
      category: CategoriesCategory,
      search: CategoriesSearch
    }
  })
};

export default CategoriesQueries;
