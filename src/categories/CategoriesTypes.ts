import {
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { CategoriesModel } from "./CategoriesModel";

export const CategoryCreateType = new GraphQLInputObjectType({
  name: "CategoryCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    parent_id: {
      type: GraphQLID
    }
  }
});

export const CategoryUpdateType = new GraphQLInputObjectType({
  name: "CategoryUpdateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    parent_id: {
      type: GraphQLID
    }
  }
});

export const CategoryType: GraphQLObjectType = new GraphQLObjectType({
  name: "CategoryType",
  fields: () => ({
    key: {
      type: new GraphQLNonNull(GraphQLID),
      async resolve({ _id }) {
        return _id;
      }
    },
    title: {
      type: new GraphQLNonNull(GraphQLString),
      async resolve({ name }) {
        return name;
      }
    },
    parent_id: {
      type: GraphQLID
    },
    children: {
      type: new GraphQLList(CategoryType),
      async resolve(root) {
        return await CategoriesModel.find({ parent_id: root._id });
      }
    }
  })
});
