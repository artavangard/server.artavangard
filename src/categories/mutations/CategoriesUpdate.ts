import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { CategoriesModel } from "../CategoriesModel";
import { CategoryUpdateType } from "../CategoriesTypes";
import { FieldConfigWithArgs } from "../../types";

const CategoriesUpdate: FieldConfigWithArgs = {
  description: "update category data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(CategoryUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await CategoriesModel.update({ _id }, data);
    return true;
  }
};

export default CategoriesUpdate;
