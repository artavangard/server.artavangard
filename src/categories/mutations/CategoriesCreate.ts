import { GraphQLID, GraphQLNonNull } from "graphql";

import { CategoriesModel } from "../CategoriesModel";
import { CategoryCreateType } from "../CategoriesTypes";
import { FieldConfigWithArgs } from "../../types";

const CategoriesCreate: FieldConfigWithArgs = {
  description: "create new category",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(CategoryCreateType)
    }
  },
  async resolve(_, { data }) {
    const newCategory = await new CategoriesModel(data).save();
    if (!newCategory) {
      return new Error("Error adding new category");
    }

    return newCategory._id;
  }
};

export default CategoriesCreate;
