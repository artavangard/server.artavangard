import * as mongoose from 'mongoose';

export interface ICategories extends mongoose.Document {
  name: string;
  parent_id: string;
  active: boolean;
}
const Schema = mongoose.Schema;

const CategoriesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  parent_id: {
    type: Schema.Types.ObjectId,
    default: null,
  },
  active: {
    type: Boolean,
    default: true,
  },
});

export const CategoriesModel = mongoose.model('Categories', CategoriesSchema);
