import { GraphQLObjectType } from 'graphql';
import CategoriesMutations from './categories/CategoriesMutations';
import ContractorsMutations from './contractors/ContractorsMutations';
import CustomersMutations from './customers/CustomersMutations';
import DepartmentsMutations from './departments/DepartmentsMutations';
import LogsMutations from './logs/LogsMutations';
import RolesMutations from './roles/RolesMutations';
import UsersMutations from './users/UsersMutations';
import OrdersMutations from './orders/OrdersMutations';

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    categories: CategoriesMutations,
    contractors: ContractorsMutations,
    customers: CustomersMutations,
    departments: DepartmentsMutations,
    logs: LogsMutations,
    roles: RolesMutations,
    users: UsersMutations,
    orders: OrdersMutations,
  }),
});

export default Mutation;
