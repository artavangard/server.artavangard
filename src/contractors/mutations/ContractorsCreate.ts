import { GraphQLID, GraphQLNonNull } from "graphql";

import { ContractorCreateType } from "../ContractorsTypes";
import { ContractorsModel } from "../ContractorsModel";
import { FieldConfigWithArgs } from "../../types";

const ContractorsCreate: FieldConfigWithArgs = {
  description: "create new contractor",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(ContractorCreateType)
    }
  },
  async resolve(_, { data }) {
    const newContractor = await new ContractorsModel(data).save();
    if (!newContractor) {
      return new Error("Error adding new contractor");
    }

    return newContractor._id;
  }
};

export default ContractorsCreate;
