import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { ContractorUpdateType } from "../ContractorsTypes";
import { ContractorsModel } from "../ContractorsModel";
import { FieldConfigWithArgs } from "../../types";

const ContractorsUpdate: FieldConfigWithArgs = {
  description: "update contractor data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(ContractorUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await ContractorsModel.update({ _id }, data);
    return true;
  }
};

export default ContractorsUpdate;
