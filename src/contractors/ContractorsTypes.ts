import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { CategoriesModel } from "../categories/CategoriesModel";
import { CategoryType } from "../categories/CategoriesTypes";
import { UserType } from "../users/UsersTypes";
import { UsersModel } from "../users/UsersModel";
import { success as signaleSuccess } from "signale";

export const ContractorType = new GraphQLObjectType({
  name: "ContractorType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    persons: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLObjectType({
            name: "ContractorPersonType",
            fields: {
              name: {
                type: new GraphQLNonNull(GraphQLString)
              },
              phone: {
                type: GraphQLString
              },
              email: {
                type: GraphQLString
              },
              main: {
                type: new GraphQLNonNull(GraphQLBoolean)
              }
            }
          })
        )
      )
    },
    rate: {
      type: new GraphQLNonNull(GraphQLInt)
    },
    categories: {
      type: new GraphQLNonNull(new GraphQLList(CategoryType)),
      async resolve({ categories }) {
        signaleSuccess(categories);
        return await CategoriesModel.find({ _id: { $in: categories } });
      }
    },
    createdManager: {
      type: new GraphQLNonNull(UserType),
      async resolve({ createdManager }) {
        signaleSuccess(createdManager);
        return await UsersModel.findOne({ _id: createdManager }).exec();
      }
    },
    created: {
      type: new GraphQLNonNull(GraphQLString)
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString)
    }
  }
});

export const ContractorCreateType = new GraphQLInputObjectType({
  name: "ContractorCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    persons: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLInputObjectType({
            name: "ContractorPersonCreateType",
            fields: {
              name: {
                type: new GraphQLNonNull(GraphQLString)
              },
              phone: {
                type: GraphQLString
              },
              email: {
                type: GraphQLString
              },
              main: {
                type: GraphQLBoolean
              }
            }
          })
        )
      )
    },
    rate: {
      type: GraphQLInt
    },
    categories: {
      type: new GraphQLList(GraphQLID)
    },
    createdManager: {
      type: new GraphQLNonNull(GraphQLID)
    }
  }
});

export const ContractorUpdateType = new GraphQLInputObjectType({
  name: "ContractorUpdateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    persons: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLInputObjectType({
            name: "ContractorPersonUpdateType",
            fields: {
              name: {
                type: new GraphQLNonNull(GraphQLString)
              },
              phone: {
                type: GraphQLString
              },
              email: {
                type: GraphQLString
              },
              main: {
                type: GraphQLBoolean
              }
            }
          })
        )
      )
    },
    rate: {
      type: GraphQLInt
    },
    categories: {
      type: new GraphQLList(GraphQLID)
    },
    createdManager: {
      type: new GraphQLNonNull(GraphQLID)
    }
  }
});
