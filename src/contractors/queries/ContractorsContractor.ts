import { GraphQLID, GraphQLNonNull } from "graphql";

import { ContractorType } from "../ContractorsTypes";
import { ContractorsModel } from "../ContractorsModel";
import { FieldConfigWithArgs } from "../../types";

const ContractorsContractor: FieldConfigWithArgs = {
  description: "find contractor by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  type: new GraphQLNonNull(ContractorType),
  async resolve(_, { _id }) {
    return await ContractorsModel.findOne(_id);
  }
};

export default ContractorsContractor;
