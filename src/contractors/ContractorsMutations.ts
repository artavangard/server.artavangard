import ContractorsCreate from "./mutations/ContractorsCreate";
import ContractorsUpdate from "./mutations/ContractorsUpdate";
import { GraphQLObjectType } from "graphql";

const ContractorsMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "ContractorsMutations",
    fields: {
      create: ContractorsCreate,
      contractor: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "ContractorMutations",
          fields: {
            update: ContractorsUpdate
          }
        })
      }
    }
  })
};

export default ContractorsMutations;
