import * as mongoose from 'mongoose';

export interface IContractors extends mongoose.Document {
  name: string;
  persons: {
    name: string;
    phone: string;
    email: string;
    main: boolean;
  }[];
  rate: number;
  categories: string[];
  createdManager: string;
  created: string;
  updated: string;
}

const Schema = mongoose.Schema;

const ContractorsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    persons: {
      type: [
        {
          name: {
            type: String,
            required: true,
          },
          phone: {
            type: String,
          },
          email: {
            type: String,
          },
          main: {
            type: Boolean,
            default: false,
          },
        },
      ],
    },
    rate: {
      type: Number,
      default: 0,
    },
    categories: {
      type: [Schema.Types.ObjectId],
      default: [],
    },
    createdManager: {
      type: Schema.Types.ObjectId,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated',
    },
  },
);

export const ContractorsModel = mongoose.model<IContractors>('Contractors', ContractorsSchema);
