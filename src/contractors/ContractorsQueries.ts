import ContractorsContractor from "./queries/ContractorsContractor";
import ContractorsSearch from "./queries/ContractorsSearch";
import { GraphQLObjectType } from "graphql";

const ContractorsQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "ContractorsQueries",
    fields: {
      contractor: ContractorsContractor,
      search: ContractorsSearch
    }
  })
};

export default ContractorsQueries;
