import { GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { UserType } from "../UsersTypes";
import { UsersModel } from "../UsersModel";

const UsersUser: FieldConfigWithArgs = {
  description: "find user by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  type: new GraphQLNonNull(UserType),
  async resolve(_, { _id }) {
    return await UsersModel.findOne({ _id });
  }
};

export default UsersUser;
