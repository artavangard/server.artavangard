import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { DepartmentsModel } from "../departments/DepartmentsModel";
import { RolesModel } from "../roles/RolesModel";

export const UserType = new GraphQLObjectType({
  name: "UserType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    login: {
      type: new GraphQLNonNull(GraphQLString)
    },
    email: {
      type: new GraphQLNonNull(GraphQLString)
    },
    created: {
      type: new GraphQLNonNull(GraphQLString)
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString)
    },
    phone: {
      type: GraphQLString
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    lastName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    department: {
      type: new GraphQLNonNull(
        new GraphQLObjectType({
          name: "UserDepartment",
          fields: {
            _id: {
              type: new GraphQLNonNull(GraphQLID),
              async resolve(root) {
                return root;
              }
            },
            name: {
              type: new GraphQLNonNull(GraphQLString),
              async resolve(root: string) {
                const department = await DepartmentsModel.findOne({
                  _id: root
                });
                return department && department.name;
              }
            }
          }
        })
      )
    },
    role: {
      type: new GraphQLNonNull(
        new GraphQLObjectType({
          name: "UserRole",
          fields: {
            _id: {
              type: new GraphQLNonNull(GraphQLID),
              async resolve(root) {
                return root;
              }
            },
            name: {
              type: new GraphQLNonNull(GraphQLString),
              async resolve(root: string) {
                const role = await RolesModel.findOne({ _id: root });
                return role && role.name;
              }
            }
          }
        })
      )
    },
    active: {
      type: GraphQLBoolean
    },
    state: {
      type: GraphQLString
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
      async resolve(root: { lastName: string; firstName: string }) {
        return root.lastName + " " + root.firstName;
      }
    }
  }
});

export const UserCreateType = new GraphQLInputObjectType({
  name: "UserCreateType",
  fields: {
    login: {
      type: new GraphQLNonNull(GraphQLString)
    },
    email: {
      type: new GraphQLNonNull(GraphQLString)
    },
    secret: {
      type: new GraphQLNonNull(
        new GraphQLInputObjectType({
          name: "UserCreateSecretType",
          fields: {
            password: {
              type: new GraphQLNonNull(GraphQLString)
            }
          }
        })
      )
    },
    phone: {
      type: GraphQLString
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    lastName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    department: {
      type: new GraphQLNonNull(GraphQLID)
    },
    role: {
      type: new GraphQLNonNull(GraphQLID)
    }
  }
});

export const UserUpdateType = new GraphQLInputObjectType({
  name: "UserUpdateType",
  fields: {
    login: {
      type: new GraphQLNonNull(GraphQLString)
    },
    email: {
      type: GraphQLString
    },
    secret: {
      type: new GraphQLInputObjectType({
        name: "UserUpdateSecretType",
        fields: {
          password: {
            type: new GraphQLNonNull(GraphQLString)
          }
        }
      })
    },
    phone: {
      type: GraphQLString
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    lastName: {
      type: new GraphQLNonNull(GraphQLString)
    },
    department: {
      type: new GraphQLNonNull(GraphQLID)
    },

    role: {
      type: new GraphQLNonNull(GraphQLID)
    },
    archive: {
      type: GraphQLBoolean
    }
  }
});

export const UserPasswordChangeType = new GraphQLInputObjectType({
  name: "UserPasswordChangeType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    oldPassword: {
      type: new GraphQLNonNull(GraphQLString)
    },
    newPassword: {
      type: new GraphQLNonNull(GraphQLString)
    }
  }
});
