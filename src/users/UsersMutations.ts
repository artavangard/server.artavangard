import { GraphQLObjectType } from "graphql";
import { UserPasswordChange } from "./mutations/UserPasswordChange";
import { UserUpdate } from "./mutations/UserUpdate";
import { UsersCreate } from "./mutations/UsersCreate";

const UsersMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "UsersMutations",
    fields: {
      create: UsersCreate,
      update: UserUpdate,
      updatePassword: UserPasswordChange
    }
  })
};

export default UsersMutations;
