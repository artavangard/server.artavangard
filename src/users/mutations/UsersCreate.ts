import * as bcrypt from "bcrypt";

import { GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { UserCreateType } from "../UsersTypes";
import { UsersModel } from "../UsersModel";
import { error as signaleError } from "signale";

export const UsersCreate: FieldConfigWithArgs = {
  description: "create new user",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(UserCreateType)
    }
  },
  async resolve(_, { data }) {
    data.secret.password = await bcrypt
      .hash(data.secret.password, 10)
      .then(hash => {
        return hash;
      })
      .catch(err => {
        signaleError(err);
        return "";
      });

    const newUser = await new UsersModel(data).save();
    if (!newUser) {
      return new Error("Error adding new user");
    }

    return newUser._id;
  }
};
