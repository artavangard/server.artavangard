import { GraphQLID, GraphQLNonNull, GraphQLString } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { UsersModel } from "../UsersModel";

export const UserStateChange: FieldConfigWithArgs = {
  description: "user state change",
  type: GraphQLString,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    state: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  async resolve(_, { _id, state }) {
    await UsersModel.update({ _id }, { state });
  }
};
