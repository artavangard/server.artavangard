import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { UserUpdateType } from "../UsersTypes";
import { UsersModel } from "../UsersModel";

export const UserUpdate: FieldConfigWithArgs = {
  description: "update User data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(UserUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await UsersModel.update({ _id }, data);
    return true;
  }
};
