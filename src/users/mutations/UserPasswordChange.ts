import * as bcrypt from "bcrypt";

import { GraphQLBoolean, GraphQLString } from "graphql/type/scalars";
import { GraphQLNonNull, GraphQLObjectType } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { UserPasswordChangeType } from "../UsersTypes";
import { UsersModel } from "../UsersModel";

export const UserPasswordChange: FieldConfigWithArgs = {
  description: "change password",
  type: new GraphQLObjectType({
    name: "response",
    fields: {
      state: {
        type: new GraphQLNonNull(GraphQLBoolean)
      },
      field: {
        type: GraphQLString
      }
    }
  }),
  args: {
    data: {
      type: new GraphQLNonNull(UserPasswordChangeType)
    }
  },
  async resolve(_, { data }) {
    const { _id, oldPassword, newPassword } = data;
    const user = await UsersModel.findById(_id);

    if (!user) {
      return { state: false };
    }

    if (!(await bcrypt.compare(oldPassword, user.secret.password))) {
      return { state: false, field: "oldPassword" };
    }

    const hashedPassword = await bcrypt
      .hash(newPassword, 10)
      .then(hash => {
        return hash;
      })
      .catch(() => {
        return new Error("Error change user password hash");
      });

    const update = await UsersModel.updateOne(
      { _id },
      { "secret.password": hashedPassword }
    );

    if (update) return { state: true };

    return false;
  }
};
