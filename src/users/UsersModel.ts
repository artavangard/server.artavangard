import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export interface IUser extends mongoose.Document {
  login: string;
  email: string;
  phone: string;
  lastName: string;
  firstName: string;
  department: string;
  role: string;
  secret: {
    password: string;
    uid: string;
  };
  active: boolean;
  state: 'in' | 'out';
  created: string;
  updated: string;
}

const UsersSchema = new mongoose.Schema(
  {
    login: {
      type: String,
      lowercase: true,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      lowercase: true,
      required: true,
      unique: true,
    },
    phone: {
      type: String,
    },
    secret: {
      password: {
        type: String,
      },
      uid: {
        type: String,
        default: '',
      },
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    department: {
      type: Schema.Types.ObjectId,
    },
    role: {
      type: Schema.Types.ObjectId,
    },
    active: {
      type: Boolean,
    },
    state: {
      type: String,
      default: 'out',
    },
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated',
    },
  },
);

export const UsersModel = mongoose.model<IUser>('Users', UsersSchema);
