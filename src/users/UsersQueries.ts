import { GraphQLObjectType } from 'graphql';
import UsersSearch from './queries/UsersSearch';
import UsersUser from './queries/UsersUser';

const UsersQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: 'UsersQueries',
    fields: {
      user: UsersUser,
      search: UsersSearch,
    },
  }),
};

export default UsersQueries;
