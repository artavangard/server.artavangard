import * as koa from "koa";
import * as koaBody from "koa-bodyparser";
import * as koaRouter from "koa-router";
import * as mongoose from "mongoose";

import { SignAuthAction, SignInAction, SignOutAction } from "./auth/Auth";
import { Signale, fatal as signaleFatal } from "signale";
import { graphiqlKoa, graphqlKoa } from "graphql-server-koa";

import { GraphQLSchema } from "graphql";
import Mutation from "./Mutation";
import Query from "./Query";

const interactive = new Signale({
  interactive: true,
  scope: "starting"
});

const PORT = process.env.PORT || 3000;
const DB_HOST = "mongodb://31.31.201.206:3010/artavangard";

(<any>mongoose).Promise = global.Promise;

mongoose
  .connect(
    DB_HOST,
    { useNewUrlParser: true }
  )
  .then(() =>
    interactive.success(`[%d/2]Соединение с сервером успешно. ${DB_HOST}`, 2)
  )
  .catch(err => {
    signaleFatal(err);
  });
const app = new koa();

const router = new koaRouter();
const schema = new GraphQLSchema({
  query: Query,
  mutation: Mutation
});

app.use(koaBody());

// autentification routes
router.get("/auth", SignAuthAction);
router.post("/signin", SignInAction);
router.post("/signout", SignOutAction);

// graphql routes
router.post("/graphql", graphqlKoa({ schema }));
router.post("/graphql/schema/", graphqlKoa({ schema }));
router.get(
  "/graphiql",
  graphiqlKoa({
    endpointURL: "/graphql"
  })
);

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(PORT, () => {
  interactive.success(
    `[%d/2]Сервер успешно запущен http://localhost:${PORT}`,
    1
  );
});
