import { bot, artChatId } from '../../bot';
import * as moment from 'moment';
moment.locale();

export const addCotractor = (id: string, name: string, manager: string) => {
  bot.sendMessage(
    artChatId,
    `Добавлен новый подрядчик: "${name}" пользователем ${manager}. Для просмотра перейдите по ссылке http://wikifela.ru/contractor/${id}`);
};
