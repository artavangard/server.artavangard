import { bot, artChatId } from "../index";
import * as moment from "moment";

moment.locale("ru_RU");

export const startWork = (id: string, name: string) => {
  bot.sendMessage(artChatId, `Сотрудник ${name} c id ${id} зачекинился в офисе!`);
};

export const endWorkTime = (id: string, name: string) => {
  bot.sendMessage(artChatId, `Сотрудник ${name} c id ${id} уходит с офиса`);
};
