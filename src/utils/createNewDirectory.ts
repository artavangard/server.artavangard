import { Signale, fatal as signaleFatal, info as signaleInfo } from "signale";

import fs from "fs-extra";
import optimist from "optimist";

const interactive = new Signale({ interactive: true, scope: "create" });

const { name } = optimist.argv as { name: string };

if (!name) {
  signaleFatal("Missing --name attribute");
  process.exit();
}

const Name = name.charAt(0).toUpperCase() + name.slice(1);

const QueriesBody = `import { GraphQLObjectType } from 'graphql';
import { ${Name}sSearch } from './queries/${Name}sSearch';
import { ${Name}s${Name} } from './queries/${Name}s${Name}';

export const ${Name}sQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: '${Name}sQueries',
    fields: {
      find: ${Name}s${Name},
      search: ${Name}sSearch,
    },
  }),
};
`;

const MutationsBody = `import { GraphQLObjectType } from 'graphql';
import { ${Name}sCreate } from './mutations/${Name}sCreate';
import { ${Name}sUpdate } from './mutations/${Name}sUpdate';

export const ${Name}sMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: '${Name}sMutations',
    fields: {
      create: ${Name}sCreate,
      ${name}: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: '${Name}Mutations',
          fields: {
            update: ${Name}sUpdate,
          },
        }),
      },
    },
  }),
};
`;

const ModelBody = `import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ${Name}sSchema = new mongoose.Schema({

});

export const ${Name}sModel = mongoose.model('${Name}s', ${Name}sSchema);
`;

const TypesBody = `import {
  GraphQLID, GraphQLInputObjectType, GraphQLList, GraphQLNonNull, GraphQLObjectType,
  GraphQLString,
} from 'graphql';

export const ${Name}Type = new GraphQLObjectType({
  name: '${Name}Type',
  fields: {
    
  },
});
 
export const ${Name}CreateType = new GraphQLInputObjectType({
  name: '${Name}CreateType',
  fields: {
  
  },
});

export const ${Name}UpdateType = new GraphQLInputObjectType({
  name: '${Name}UpdateType',
  fields: {
    
  },
});
`;

const FindByIdBody = `import { GraphQLNonNull, GraphQLString } from 'graphql';
import { ${Name}Type } from '../${Name}sTypes';
import { ${Name}sModel } from '../${Name}sModel';

export const ${Name}s${Name} = {
  description: 'find ${name} by id',
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  type: new GraphQLNonNull(${Name}Type),
  async resolve(root, { _id }, options) {
    return (
      await ${Name}sModel.findOne(_id)
    );
  },
};
`;

const FindSearchBody = `import { GraphQLInt, GraphQLList, GraphQLNonNull, 
  GraphQLObjectType, GraphQLString } from 'graphql';
import { ${Name}Type } from '../${Name}sTypes';
import { ${Name}sModel } from '../${Name}sModel';

export const ${Name}sSearch = {
  description: 'find ${name}',
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10,
    },
    after: {
      type: GraphQLString,
    },
    sort: {
      type: GraphQLString,
    },
    order: {
      type: GraphQLInt,
    },
  },
  type: new GraphQLNonNull(new GraphQLObjectType({
    name: '${Name}sSearch',
    fields: {
      node: {
        type: new GraphQLNonNull(new GraphQLList(${Name}Type)),
      },
      info: {
        type: new GraphQLNonNull(new GraphQLObjectType({
          name: '${Name}sSearchInfo',
          fields: {
            count: {
              type: new GraphQLNonNull(GraphQLInt),
            },
            cursor: {
              type: new GraphQLNonNull(GraphQLString),
            },
          },
        })),
      },
    },
  })),
  async resolve(root, { limit, after, sort, order }, options) {
    const find:any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await ${Name}sModel.find()
      .count();

    const result = await (
      await ${Name}sModel.find(find)
        .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
        .limit(limit as any)
        .exec()
    );

    let cursor = '';
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count,
      },
    };
  },
};
`;

const CreateBody = `import { GraphQLID, GraphQLNonNull } from 'graphql';
import { ${Name}CreateType } from '../${Name}sTypes';
import { ${Name}sModel } from '../${Name}sModel';

export const ${Name}sCreate = {
  description: 'create new ${name}',
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(${Name}CreateType),
    },
  },
  async resolve(parent, { data }, options) {
    const new${Name} = await new ${Name}sModel(data).save();
    if (!new${Name}) {
      return new Error('Error adding new ${name}');
    }

    return new${Name}._id;
  },
};
`;

const UpdateBody = `import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from 'graphql';
import { ${Name}UpdateType } from '../${Name}sTypes';
import { ${Name}sModel } from '../${Name}sModel';

export const ${Name}sUpdate = {
  description: 'update ${name} data',
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    data: {
      type: new GraphQLNonNull(${Name}UpdateType),
    },
  },
  async resolve(parent, { data, _id }, options) {

    await ${Name}sModel.update({ _id }, data);
    return true;
  },
};
`;

const stopProcess = (name: string, err?: string) => {
  signaleFatal(`Произошла ошибка при создании каталога или файла ${name}`);
  signaleInfo(err);
  fs.remove(`./src/${name}s`).then(() => signaleInfo("Директория удалена"));
  process.exit();
};

const createDir = (name: string) => {
  fs.mkdir(name)
    .then(() => interactive.success(`create directory ${name} success!`))
    .catch(err => stopProcess(name, err));
};

const createFile = (path: string, body: string) => {
  fs.writeFile(path, body)
    .then(() => interactive.success(`create file ${name} success!`))
    .catch(err => stopProcess(name, err));
};

const createFiles = async () => {
  await createDir(`./src/${name}s`);
  await createDir(`./src/${name}s/queries`);
  await createDir(`./src/${name}s/mutations`);

  createFile(`./src/${name}s/${Name}sQueries.ts`, QueriesBody);
  createFile(`./src/${name}s/queries/${Name}s${Name}.ts`, FindByIdBody);
  createFile(`./src/${name}s/queries/${Name}sSearch.ts`, FindSearchBody);

  createFile(`./src/${name}s/${Name}sMutations.ts`, MutationsBody);
  createFile(`./src/${name}s/mutations/${Name}sCreate.ts`, CreateBody);
  createFile(`./src/${name}s/mutations/${Name}sUpdate.ts`, UpdateBody);

  createFile(`./src/${name}s/${Name}sModel.ts`, ModelBody);
  createFile(`./src/${name}s/${Name}sTypes.ts`, TypesBody);
};

createFiles();
