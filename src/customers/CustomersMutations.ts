import CustomersCreate from "./mutations/CustomersCreate";
import CustomersUpdate from "./mutations/CustomersUpdate";
import { GraphQLObjectType } from "graphql";

const CustomersMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "CustomersMutations",
    fields: {
      create: CustomersCreate,
      customer: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "CustomerMutations",
          fields: {
            update: CustomersUpdate
          }
        })
      }
    }
  })
};

export default CustomersMutations;
