import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { CustomerType } from "../CustomersTypes";
import { CustomersModel } from "../CustomersModel";
import { FieldConfigWithArgs } from "../../types";

const CustomersSearch: FieldConfigWithArgs = {
  description: "find customer",
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "CustomersSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(
            new GraphQLList(new GraphQLNonNull(CustomerType))
          )
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "CustomersSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve(_, { limit, after, sort, order }) {
    const find: any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await CustomersModel.find().count();

    const result = await await CustomersModel.find(find)
      .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
      .limit(limit as any)
      .exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default CustomersSearch;
