import { GraphQLID, GraphQLNonNull } from "graphql";

import { CustomerType } from "../CustomersTypes";
import { CustomersModel } from "../CustomersModel";
import { FieldConfigWithArgs } from "../../types";

const CustomersCustomer: FieldConfigWithArgs = {
  description: "find customer by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  type: new GraphQLNonNull(CustomerType),
  async resolve(_, { _id }) {
    return await CustomersModel.findOne({ _id });
  }
};

export default CustomersCustomer;
