import * as mongoose from 'mongoose';

export interface ICustomers extends mongoose.Document {
  name: string;
  manager: string;
  person: string;
  personEmail: string;
  personPhone: string;
  ltd: boolean;
  INN: string;
  OGRN: string;
  KPP: string;
  RS: string;
  KS: string;
  BIK: string;
  Bank: string;
  created: string;
  updated: string;
}

const Schema = mongoose.Schema;

const CustomersSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    manager: {
      type: Schema.Types.ObjectId,
    },
    person: {
      type: String,
    },
    personEmail: {
      type: String,
    },
    personPhone: {
      type: String,
    },
    ltd: {
      type: Boolean,
    },
    INN: {
      type: String,
    },
    OGRN: {
      type: String,
    },
    KPP: {
      type: String,
    },
    RS: {
      type: String,
    },
    KS: {
      type: String,
    },
    BIK: {
      type: String,
    },
    Bank: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated',
    },
  },
);

export const CustomersModel = mongoose.model<ICustomers>('Customers', CustomersSchema);
