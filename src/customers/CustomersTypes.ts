import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { UsersModel } from "../users/UsersModel";

export const CustomerType = new GraphQLObjectType({
  name: "CustomerType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    manager: {
      type: new GraphQLNonNull(
        new GraphQLObjectType({
          name: "CustomerManager",
          fields: {
            _id: {
              type: new GraphQLNonNull(GraphQLID),
              async resolve(root) {
                return root;
              }
            },
            name: {
              type: new GraphQLNonNull(GraphQLString),
              async resolve(root: string) {
                const manager = await UsersModel.findOne({ _id: root });
                if (manager) {
                  return manager.lastName + " " + manager.firstName;
                }
                return false;
              }
            }
          }
        })
      )
    },
    person: {
      type: new GraphQLNonNull(GraphQLString)
    },
    personEmail: {
      type: GraphQLString
    },
    personPhone: {
      type: GraphQLString
    },
    ltd: {
      type: new GraphQLNonNull(GraphQLBoolean)
    },
    INN: {
      type: GraphQLString
    },
    OGRN: {
      type: GraphQLString
    },
    KPP: {
      type: GraphQLString
    },
    RS: {
      type: GraphQLString
    },
    KS: {
      type: GraphQLString
    },
    BIK: {
      type: GraphQLString
    },
    Bank: {
      type: GraphQLString
    }
  }
});

export const CustomerCreateType = new GraphQLInputObjectType({
  name: "CustomerCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    manager: {
      type: new GraphQLNonNull(GraphQLID)
    },
    person: {
      type: new GraphQLNonNull(GraphQLString)
    },
    personEmail: {
      type: GraphQLString
    },
    personPhone: {
      type: GraphQLString
    },
    ltd: {
      type: new GraphQLNonNull(GraphQLBoolean)
    },
    INN: {
      type: GraphQLString
    },
    OGRN: {
      type: GraphQLString
    },
    KPP: {
      type: GraphQLString
    },
    RS: {
      type: GraphQLString
    },
    KS: {
      type: GraphQLString
    },
    BIK: {
      type: GraphQLString
    },
    Bank: {
      type: GraphQLString
    }
  }
});

export const CustomerUpdateType = new GraphQLInputObjectType({
  name: "CustomerUpdateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    manager: {
      type: new GraphQLNonNull(GraphQLID)
    },
    person: {
      type: new GraphQLNonNull(GraphQLString)
    },
    personEmail: {
      type: GraphQLString
    },
    personPhone: {
      type: GraphQLString
    },
    ltd: {
      type: new GraphQLNonNull(GraphQLBoolean)
    },
    INN: {
      type: GraphQLString
    },
    OGRN: {
      type: GraphQLString
    },
    KPP: {
      type: GraphQLString
    },
    RS: {
      type: GraphQLString
    },
    KS: {
      type: GraphQLString
    },
    BIK: {
      type: GraphQLString
    },
    Bank: {
      type: GraphQLString
    }
  }
});
