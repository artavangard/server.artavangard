import { GraphQLID, GraphQLNonNull } from "graphql";

import { CustomerCreateType } from "../CustomersTypes";
import { CustomersModel } from "../CustomersModel";
import { FieldConfigWithArgs } from "../../types";

const CustomersCreate: FieldConfigWithArgs = {
  description: "create new customer",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(CustomerCreateType)
    }
  },
  async resolve(_, { data }) {
    const newCustomer = await new CustomersModel(data).save();
    if (!newCustomer) {
      return new Error("Error adding new customer");
    }

    return newCustomer._id;
  }
};

export default CustomersCreate;
