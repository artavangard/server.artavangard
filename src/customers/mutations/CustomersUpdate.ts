import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { CustomerUpdateType } from "../CustomersTypes";
import { CustomersModel } from "../CustomersModel";
import { FieldConfigWithArgs } from "../../types";

const CustomersUpdate: FieldConfigWithArgs = {
  description: "update customer data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(CustomerUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await CustomersModel.update({ _id }, data);
    return true;
  }
};

export default CustomersUpdate;
