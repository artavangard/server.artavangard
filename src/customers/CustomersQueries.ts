import CustomersCustomer from "./queries/CustomersCustomer";
import CustomersSearch from "./queries/CustomersSearch";
import { GraphQLObjectType } from "graphql";

const CustomersQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "CustomersQueries",
    fields: {
      customer: CustomersCustomer,
      search: CustomersSearch
    }
  })
};

export default CustomersQueries;
