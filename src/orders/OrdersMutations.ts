import { GraphQLObjectType } from "graphql";
import OrdersCreate from "./mutations/OrdersCreate";
import OrdersUpdate from "./mutations/OrdersUpdate";

const OrdersMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "OrdersMutations",
    fields: {
      create: OrdersCreate,
      order: {
        resolve() {
          return {};
        },
        type: new GraphQLObjectType({
          name: "OrderMutations",
          fields: {
            update: OrdersUpdate
          }
        })
      }
    }
  })
};

export default OrdersMutations;
