import { GraphQLObjectType } from "graphql";
import OrdersOrder from "./queries/OrdersOrder";
import OrdersSearch from "./queries/OrdersSearch";

const OrdersQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "OrdersQueries",
    fields: {
      order: OrdersOrder,
      search: OrdersSearch
    }
  })
};

export default OrdersQueries;
