import { GraphQLBoolean, GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { OrderUpdateType } from "../OrdersTypes";
import { OrdersModel } from "../OrdersModel";

const OrdersUpdate: FieldConfigWithArgs = {
  description: "update order data",
  type: GraphQLBoolean,
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    data: {
      type: new GraphQLNonNull(OrderUpdateType)
    }
  },
  async resolve(_, { data, _id }) {
    await OrdersModel.update({ _id }, data);
    return true;
  }
};

export default OrdersUpdate;
