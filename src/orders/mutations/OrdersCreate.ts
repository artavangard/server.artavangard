import { GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { OrderCreateType } from "../OrdersTypes";
import { OrdersModel } from "../OrdersModel";

const OrdersCreate: FieldConfigWithArgs = {
  description: "create new order",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(OrderCreateType)
    }
  },
  async resolve(_, { data }) {
    const newOrder = await new OrdersModel(data).save();
    if (!newOrder) {
      return new Error("Error adding new order");
    }

    return newOrder._id;
  }
};

export default OrdersCreate;
