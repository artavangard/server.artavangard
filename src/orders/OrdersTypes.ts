import {
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

export const OrderType = new GraphQLObjectType({
  name: 'OrderType',
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    manager: {
      type: new GraphQLNonNull(GraphQLString),
    },
    customer: {
      type: new GraphQLNonNull(GraphQLID),
    },
    stages: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLObjectType({
            name: 'OrderStageType',
            fields: {
              _id: {
                type: new GraphQLNonNull(GraphQLID),
              },
              type: {
                type: new GraphQLNonNull(GraphQLID),
              },
              after: {
                type: GraphQLID,
              },
              contractor: {
                type: GraphQLID,
              },
              department: {
                type: GraphQLID,
              },
              state: {
                type: new GraphQLNonNull(GraphQLInt),
              },
            },
          }),
        ),
      ),
    },
    created: {
      type: new GraphQLNonNull(GraphQLString),
    },
    updated: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
});

export const OrderCreateType = new GraphQLInputObjectType({
  name: 'OrderCreateType',
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString),
    },
    manager: {
      type: new GraphQLNonNull(GraphQLID),
    },
    customer: {
      type: new GraphQLNonNull(GraphQLID),
    },
    stages: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLInputObjectType({
            name: 'OrderStageCreateType',
            fields: {
              type: {
                type: new GraphQLNonNull(GraphQLID),
              },
              after: {
                type: GraphQLID,
              },
              contractor: {
                type: GraphQLID,
              },
              department: {
                type: GraphQLID,
              },
            },
          }),
        ),
      ),
    },
  },
});

export const OrderUpdateType = new GraphQLInputObjectType({
  name: 'OrderUpdateType',
  fields: {
    name: {
      type: GraphQLString,
    },
    manager: {
      type: new GraphQLNonNull(GraphQLID),
    },
    customer: {
      type: new GraphQLNonNull(GraphQLID),
    },
    stages: {
      type: new GraphQLNonNull(
        new GraphQLList(
          new GraphQLInputObjectType({
            name: 'OrderStageUpdateType',
            fields: {
              type: {
                type: new GraphQLNonNull(GraphQLID),
              },
              after: {
                type: GraphQLID,
              },
              contractor: {
                type: GraphQLID,
              },
              department: {
                type: GraphQLID,
              },
            },
          }),
        ),
      ),
    },
  },
});
