import * as mongoose from 'mongoose';

export interface IOrders extends mongoose.Document {
  name: string;
  manager: string;
  customer: string;
  stages: {
    type: string;
    after: string;
    contractor: string;
    department: string;
  }[];
  created: string;
  updated: string;
}
const Schema = mongoose.Schema;

const OrdersSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    manager: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    customer: {
      type: Schema.Types.ObjectId,
    },
    stages: [
      {
        type: {
          type: Schema.Types.ObjectId,
          required: true,
        },
        after: {
          type: Schema.Types.ObjectId,
        },
        contractor: {
          type: Schema.Types.ObjectId,
        },
        department: {
          type: Schema.Types.ObjectId,
        },
      },
    ],
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated',
    },
  },
);

export const OrdersModel = mongoose.model<IOrders>('Orders', OrdersSchema);
