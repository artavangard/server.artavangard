import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { OrderType } from "../OrdersTypes";
import { OrdersModel } from "../OrdersModel";

const OrdersSearch: FieldConfigWithArgs = {
  description: "find order",
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 10
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "OrdersSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(new GraphQLList(OrderType))
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "OrdersSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve(_, { limit, after, sort, order }) {
    const find: any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await OrdersModel.find().count();

    const result = await await OrdersModel.find(find)
      .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
      .limit(limit as any)
      .exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default OrdersSearch;
