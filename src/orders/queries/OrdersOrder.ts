import { GraphQLNonNull, GraphQLString } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { OrderType } from "../OrdersTypes";
import { OrdersModel } from "../OrdersModel";

const OrdersOrder: FieldConfigWithArgs = {
  description: "find order by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  type: new GraphQLNonNull(OrderType),
  async resolve(_, { _id }) {
    return await OrdersModel.findOne(_id);
  }
};

export default OrdersOrder;
