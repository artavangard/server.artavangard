import { GraphQLFieldConfig } from "graphql";

declare type QueryParameters = {
  limit: number;
  after: number;
  sort: string;
  order: number;
  _id: string;
  data: any;
  state: string;
};

declare type FieldConfigWithArgs = GraphQLFieldConfig<
  any,
  any,
  Partial<QueryParameters>
>;
