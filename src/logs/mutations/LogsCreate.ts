import { GraphQLID, GraphQLNonNull } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { LogCreateType } from "../LogsTypes";
import { LogsModel } from "../LogsModel";

const LogsCreate: FieldConfigWithArgs = {
  description: "create new log",
  type: GraphQLID,
  args: {
    data: {
      type: new GraphQLNonNull(LogCreateType)
    }
  },
  async resolve(_, { data }) {
    const newLog = await new LogsModel(data).save();
    if (!newLog) {
      return new Error("Error adding new log");
    }

    return newLog._id;
  }
};

export default LogsCreate;
