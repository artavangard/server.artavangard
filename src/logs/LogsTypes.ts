import {
  GraphQLID,
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { UserType } from "../users/UsersTypes";
import { UsersModel } from "../users/UsersModel";

export const LogType = new GraphQLObjectType({
  name: "LogType",
  fields: {
    _id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    user: {
      type: new GraphQLNonNull(UserType),
      async resolve({ user }) {
        return await UsersModel.find({ _id: user });
      }
    },
    info: {
      type: GraphQLString
    }
  }
});

export const LogCreateType = new GraphQLInputObjectType({
  name: "LogCreateType",
  fields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    user: {
      type: new GraphQLNonNull(GraphQLID)
    },
    info: {
      type: GraphQLString
    }
  }
});
