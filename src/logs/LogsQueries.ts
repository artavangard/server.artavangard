import { GraphQLObjectType } from "graphql";
import LogsLog from "./queries/LogsLog";
import LogsSearch from "./queries/LogsSearch";

const LogsQueries = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "LogsQueries",
    fields: {
      log: LogsLog,
      search: LogsSearch
    }
  })
};

export default LogsQueries;
