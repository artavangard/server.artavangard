import { GraphQLNonNull, GraphQLString } from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { LogType } from "../LogsTypes";
import { LogsModel } from "../LogsModel";

const LogsLog: FieldConfigWithArgs = {
  description: "find log by id",
  args: {
    _id: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  type: new GraphQLNonNull(LogType),
  async resolve(_, { _id }) {
    return await LogsModel.findOne(_id);
  }
};

export default LogsLog;
