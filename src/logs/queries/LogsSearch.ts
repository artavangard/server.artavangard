import {
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString
} from "graphql";

import { FieldConfigWithArgs } from "../../types";
import { LogType } from "../LogsTypes";
import { LogsModel } from "../LogsModel";

const LogsSearch: FieldConfigWithArgs = {
  description: "find log",
  args: {
    limit: {
      type: GraphQLInt,
      defaultValue: 50
    },
    after: {
      type: GraphQLString
    },
    sort: {
      type: GraphQLString
    },
    order: {
      type: GraphQLInt
    }
  },
  type: new GraphQLNonNull(
    new GraphQLObjectType({
      name: "LogsSearch",
      fields: {
        node: {
          type: new GraphQLNonNull(new GraphQLList(LogType))
        },
        info: {
          type: new GraphQLNonNull(
            new GraphQLObjectType({
              name: "LogsSearchInfo",
              fields: {
                count: {
                  type: new GraphQLNonNull(GraphQLInt)
                },
                cursor: {
                  type: new GraphQLNonNull(GraphQLString)
                }
              }
            })
          )
        }
      }
    })
  ),
  async resolve(_, { limit, after, sort, order }) {
    const find: any = {};

    if (after) {
      find._id = (order || -1) > 0 ? { $gt: after } : { $lt: after };
    }

    const count = await LogsModel.find().count();

    const result = await await LogsModel.find(find)
      .sort(sort ? { [sort]: order || 1 } : { _id: -1 })
      .limit(limit as any)
      .exec();

    let cursor = "";
    if (result.length > 0) {
      cursor = result[result.length - 1]._id.toHexString();
    }

    return {
      node: result,
      info: {
        cursor,
        count
      }
    };
  }
};

export default LogsSearch;
