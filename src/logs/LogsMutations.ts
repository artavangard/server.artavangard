import { GraphQLObjectType } from "graphql";
import LogsCreate from "./mutations/LogsCreate";

const LogsMutations = {
  resolve() {
    return {};
  },
  type: new GraphQLObjectType({
    name: "LogsMutations",
    fields: {
      create: LogsCreate
    }
  })
};

export default LogsMutations;
