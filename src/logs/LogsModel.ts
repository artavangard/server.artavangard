import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const LogsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    info: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: 'created',
    },
  },
);

export const LogsModel = mongoose.model('Logs', LogsSchema);
